# Calculator sample

## What is it for?

This is a sample Calculator project, it is needed to:

- Quick start with PHP projects.
- Understand basic approaches.

## How to use?

There are two ways:

- Import/open this project in PHP Storm.
- Edit the project the "old school" way (with any text editor) :).
